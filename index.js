import React from "react";
import ReactDOM from "react-dom";
import Reg from "./Reg";
import Login from "./Login";
import "./styles.css";

function App() {
  return (
    <div className="App">
      <Reg />
      <Login />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
